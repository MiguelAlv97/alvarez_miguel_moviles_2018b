package com.miguelesteban.examenandroid2018b

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main_examen.*

class MainActivityExamen : AppCompatActivity() {
    internal lateinit var txtViewPuntaje: TextView
    internal lateinit var txtViewNumeroRandom: TextView
    internal lateinit var ButtonIniciar: Button
    internal lateinit var ButtonSuerte: Button
    internal lateinit var countDownTimer: CountDownTimer
    internal val countDownInterval = 1000L
    internal val initialCountDown = 10000L
    internal val TAG = MainActivityExamen::class.java.simpleName

    internal var score = 0
    internal var gameStarted = false
    internal var timeleft = 10
    internal var numeroRandom = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_examen)
        //Log.d(TAG, 'onCreate . score is $score');
        Log.d(TAG, "onCreate . score is $score")
        txtViewPuntaje = findViewById(R.id.textViewPuntaje)
        txtViewNumeroRandom = findViewById(R.id.textViewRandom)
        ButtonIniciar = findViewById(R.id.buttonInicio)
        ButtonSuerte = findViewById(R.id.buttonSuerte)
        ButtonIniciar.setOnClickListener{ _ -> empezarJuego()}
        ButtonSuerte.setOnClickListener{ _ -> aumentarElPuntaje()}
        reiniciarJuego()
    }
    private fun empezarJuego(){
        iniciarTimer();
        if(numeroRandom==0){
            countDownTimer.start();
            numeroRandom = (0.. 10).shuffled().first();
            txtViewNumeroRandom.text = getString(R.string.TextViewNumeroRandom, numeroRandom.toString())
            gameStarted = true;
        }else{
            Toast.makeText(
                this,
                getString(R.string.game_over_message),
                Toast.LENGTH_LONG).show();
        }
    }
    private fun aumentarElPuntaje(){
        if(numeroRandom == timeleft){
            score += 100
        }else if (numeroRandom <= timeleft+1 && numeroRandom >= timeleft-1){
            score += 50
        }else{
            score += 0
        }
        txtViewPuntaje.text = getString(R.string.TextViewPuntaje, score.toString())

        if(!gameStarted){
            empezarJuego()
        }
        countDownTimer.cancel()
        terminarJuego()
    }
    private fun iniciarTimer(){
        countDownTimer = object : CountDownTimer(initialCountDown, countDownInterval){
            override fun onTick(millisUntilFinished: Long) {
                timeleft = millisUntilFinished.toInt() / 1000;
            }

            override fun onFinish() {
                terminarJuego()
            }
        }
    }
    private fun reiniciarJuego(){
        timeleft = 10
        numeroRandom = 0
        textViewRandom.text = "☺"
        gameStarted = false
    }
    private fun terminarJuego(){
        if(gameStarted) {
            Toast.makeText(
                this,
                getString(R.string.game_over_message, Integer.toString(score)),
                Toast.LENGTH_LONG).show()
            countDownTimer.cancel()
            reiniciarJuego()
        }
    }

}
